## Reference

- https://www.home-assistant.io/installation/linux#install-home-assistant-container
- https://hub.docker.com/r/homeassistant/home-assistant


## Setup

1. Install manifests.

```bash
kubectl apply -f manifests/
```

2. Edit configuration.

```bash
# Enter container
kubectl -n home-assistant exec -it <pod_name> -- /bin/bash

# Edit config
vi /config/configuration.yaml
```

Add the following lines to allow home assistant to be accessed behind traefik reverse proxy.

```yaml
http:
  use_x_forwarded_for: true
  trusted_proxies:
  - 10.42.0.0/24
```

Restart the pod.

```bash
kubectl -n home-assistant delete pod <pod_name>
```

3. Add secret for healthcheck. Startup and liveness probes can be commented out during first start to get the apikey first.

```bash
# Create long-lived token in home assistant
read -s apikey
kubectl -n home-assistant create secret generic apikey --from-literal=BEARER_TOKEN=${apikey}
```

## Remove Sidebar Entries

Long press the word "Home Assistant" at the top of the sidebar. Individual items can be removed on a per-user basis.


## Custom Components

### NEA SG Weather

References: https://github.com/liangleslie/nea_sg_weather


## Add Sidebar Panels

Reference: https://smarthomescene.com/guides/sidebar-shortcuts-customizing-the-ha-sidebar/

```yaml
# Custom sidebar entries
panel_custom:
- name: automations
  sidebar_title: Automations
  sidebar_icon: mdi:arrow-decision
  js_url: /api/hassio/app/entrypoint.js
  url_path: 'config/automation/dashboard'
  embed_iframe: true
  require_admin: true
  config:
    ingress: core_configurator
```


## Sonoff Zigbee 3.0 USB Dongle Plus Setup

1. Passthrough device to VM via USB device passthrough.

2. Mount USB path into container.

3. `Zigbee Home Automation` integration will be detected under `settings` ➔ `integrations`. Press the  `configure` button, then `Create a network`. There will be an error, but we can ignore it.

4. Click `configure` again and click `use existing network settings`.

5. Pair the sensor to the controller. 
      - Click `settings` ➔ `Devices and Services`. Click `devices` for ZHA.
      - Click `Add Device` button. It will start scanning for the sensor.
      - Reset the sensor by pressing the reset button (below main unit) for 3s. Do not need to wait for lights to blink after 3s. It should get picked up.

### Reconnect Sensors

Sensors will disconnect after a long power outage. In order to reconnect, try the following:

- try to add devices from ZHA and reset sensor by pressing the reset button for 3s
- delete the sensor and try again
- delete the sensor and zha, then restart home assistant and perform a fresh install as per instructions above
- if still having trouble reconnecting door sensor, try opening the door to move the sensor location. Can also try moving the router further out.

### Passthrough USB device without privilege containers

Reference: 
- [smarter-device-manager](https://gitlab.com/arm-research/smarter/smarter-device-manager)
- [github issue comment giving instructions on usage](https://github.com/kubernetes/kubernetes/issues/7890#issuecomment-766088805)

There is no official way to pass `/dev/ttyUSB0` into the container like in Docker using the `--device` flag. The most straightforward way is to use privileged containers, but this is a security issue. The `smarter-device-manager` project provides a safe way to pass devices into the container.

For each desired device to passthrough, set an entry in the configmap.

```yaml
- devicematch: ^ttyUSB0$
  nummaxdevices: 1
```

Then set the device request in the pod that needs access to the device.

```yaml
resources:
  limits:
    smarter-devices/ttyUSB0: 1
  requests:
    smarter-devices/ttyUSB0: 1
```


## Telegram Integration

Reference:
- https://whatsmarthome.com/home-assistant-telegram-guide/
- https://siytek.com/home-assistant-telegram-bot/


## Delete Not Running Pods

```bash
kubectl -n home-assistant delete pods --field-selector status.phase!=Running
```