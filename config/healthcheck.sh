#!/bin/bash

set -euo pipefail                                                                                                                                                                                      

# workaround that entity becomes unavailable after crash and restart
# restarting will resolve the issue

# get entity status
results=$(curl -fsSL -H "Authorization: Bearer ${BEARER_TOKEN}" -H "Content-Type: application/json" http://127.0.0.1:8123/api/states/cover.bedroom_blind1 | jq -r '.state')

if [[ "unavailable" == "$results" ]]; then
  # entity is unavailable
  exit 1
fi

exit 0